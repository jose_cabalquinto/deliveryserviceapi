<?php

    //headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: PUT');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');
    
    //initializing api
    include_once('../../classes/initialize.php');

    //instantiating point class
    $point = new Point($db);

    $data = json_decode(file_get_contents('php://input'));

    $data = json_decode(file_get_contents('php://input'));

    $point->name              =   $data->name;
    $point->username          =   $data->username;
    $point->id              =   $data->id;

    echo json_encode($point->update());
