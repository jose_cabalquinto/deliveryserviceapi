<?php

    //headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    //initializing api
    include_once('../../classes/initialize.php');

    //instantiating point class
    $point = new Point($db);

    echo json_encode($point->read_all());