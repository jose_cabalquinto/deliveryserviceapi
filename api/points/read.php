<?php

    //headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    //initializing api
    include_once('../../classes/initialize.php');

    if(isset($_GET['id']) && isset($_GET['username'])) {
        //instantiating post class
        $point = new Point($db);

        $point->id        =   $_GET['id'];
        $point->username  =   $_GET['username'];

        echo json_encode($point->read());

    } else {
        echo json_encode(array(
            'status'     =>  false,
            'message'   =>  'Please provide username and point id.'
        ));
    }