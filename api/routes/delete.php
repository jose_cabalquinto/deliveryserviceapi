<?php

    //headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: DELETE');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');
    
    //initializing api
    include_once('../../classes/initialize.php');

    //instantiating post class
    $route = new Route($db);

    if(isset($_GET['id']) && isset($_GET['username'])) {
        //instantiating route class
        $route = new Route($db);
        $route->id           = $_GET['id'];
        $route->username     = $_GET['username'];
        echo json_encode($result = $route->delete());
    } else {
        echo json_encode(array(
            'status'    =>  false,
            'message'   =>  'Please provide route id and username.'
        ));
    }