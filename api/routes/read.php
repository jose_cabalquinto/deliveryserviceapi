<?php

    //headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    //initializing api
    include_once('../../classes/initialize.php');


    if(isset($_GET['origin_id']) && isset($_GET['destination_id'])) {
        //instantiating route class
        $route = new Route($db);
        $route->origin_id           = $_GET['origin_id'];
        $route->destination_id      = $_GET['destination_id'];
        $result = $route->get_routes();
    } else {
        echo json_encode(array(
            'status'     =>  false,
            'message'   =>  'Please provide origin and destination id.'
        ));
    }