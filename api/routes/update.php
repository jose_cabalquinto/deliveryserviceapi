<?php

    //headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: PUT');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');
    
    //initializing api
    include_once('../../classes/initialize.php');

    //instantiating point class
    $route = new Route($db);

    $data = json_decode(file_get_contents('php://input'));

    $route->origin_id           =   $data->origin_id;
    $route->destination_id      =   $data->destination_id;
    $route->cost                =   $data->cost;
    $route->time_               =   $data->time_;
    $route->username            =   $data->username;
    $route->id                  =   $data->id;

    echo json_encode($route->update());
