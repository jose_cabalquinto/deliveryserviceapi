-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2020 at 04:41 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `deliveryserviceapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `points`
--

CREATE TABLE `points` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `points`
--

INSERT INTO `points` (`id`, `name`) VALUES
(1, 'A'),
(2, 'C'),
(3, 'H'),
(4, 'E'),
(5, 'D'),
(6, 'F'),
(7, 'I'),
(8, 'B'),
(9, 'G');

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE `routes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `origin_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `cost` double NOT NULL,
  `time_` double NOT NULL,
  `history_arr` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `routes`
--

INSERT INTO `routes` (`id`, `name`, `origin_id`, `destination_id`, `cost`, `time_`, `history_arr`) VALUES
(1, 'A-C', 1, 2, 20, 1, NULL),
(2, 'C-B', 2, 8, 12, 1, '[[{\"id\":[1,2,8],\"code\":\"A-C-B\"}]]'),
(3, 'A-H', 1, 3, 1, 10, NULL),
(4, 'A-E', 1, 4, 5, 30, NULL),
(5, 'H-E', 3, 4, 1, 30, '[[{\"id\":[1,3,4],\"code\":\"A-H-E\"}]]'),
(6, 'E-D', 4, 5, 5, 3, '[[{\"id\":[1,4,5],\"code\":\"A-E-D\"}],[{\"id\":[1,3,4,5],\"code\":\"A-H-E-D\"}]]'),
(7, 'D-F', 5, 6, 50, 4, '[[{\"id\":[1,4,5,6],\"code\":\"A-E-D-F\"}],[{\"id\":[1,3,4,5,6],\"code\":\"A-H-E-D-F\"}]]'),
(8, 'F-I', 6, 7, 50, 45, '[[{\"id\":[1,4,5,6,7],\"code\":\"A-E-D-F-I\"}],[{\"id\":[1,3,4,5,6,7],\"code\":\"A-H-E-D-F-I\"}]]'),
(9, 'I-B', 7, 8, 5, 65, '[[{\"id\":[1,4,5,6,7,8],\"code\":\"A-E-D-F-I-B\"}],[{\"id\":[1,3,4,5,6,7,8],\"code\":\"A-H-E-D-F-I-B\"}]]'),
(10, 'F-G', 6, 9, 50, 40, '[[{\"id\":[1,4,5,6,9],\"code\":\"A-E-D-F-G\"}],[{\"id\":[1,3,4,5,6,9],\"code\":\"A-H-E-D-F-G\"}]]'),
(11, 'G-B', 9, 8, 74, 64, '[[{\"id\":[1,4,5,6,9,8],\"code\":\"A-E-D-F-G-B\"}],[{\"id\":[1,3,4,5,6,9,8],\"code\":\"A-H-E-D-F-G-B\"}]]');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(1, 'admin', '$2y$10$tbkWS9sOXz0dvbEKfEITSeUzM7zjZ/pMSI/3skJuKzdI4v65aovsu', 1),
(2, 'user', '$2y$10$xBSRJ4Z6cyiB40Zy9StkvOaytRxde2L94VeixyUCmvA7T0UWdYJAq', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `points`
--
ALTER TABLE `points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `origin_id` (`origin_id`),
  ADD KEY `destination_id` (`destination_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `points`
--
ALTER TABLE `points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `routes`
--
ALTER TABLE `routes`
  ADD CONSTRAINT `routes_ibfk_1` FOREIGN KEY (`origin_id`) REFERENCES `points` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `routes_ibfk_2` FOREIGN KEY (`destination_id`) REFERENCES `points` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
