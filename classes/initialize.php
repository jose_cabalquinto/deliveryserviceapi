<?php
    
    defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);   //defining directory separator
    defined('SITE_ROOT') ? null : define('SITE_ROOT', DS.'xampp'.DS.'htdocs'.DS.'deliveryserviceapi');  //defining site directory
    defined('INC_PATH') ? null : define('INC_PATH', SITE_ROOT.DS.'includes');    //defining all include files in a constant
    defined('CLASSES_PATH') ? null : define('CLASSES_PATH', SITE_ROOT.DS.'classes');    //defining all classes files in a constant

    require_once(INC_PATH.DS.'config.php');

    require_once(CLASSES_PATH.DS.'routes.php');
    require_once(CLASSES_PATH.DS.'points.php');