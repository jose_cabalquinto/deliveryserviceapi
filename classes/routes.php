<?php
    class Route {
        private $conn;
        private $table = 'routes';

        public $id;
        public $name;
        public $origin_id;
        public $destination_id;
        public $cost;
        public $time_;
        public $username;
        public $digging = false;


        public function __construct($db) {
            $this->conn = $db;
        }

        public function get_routes() {
            $arr_av_routes = array();
            $query = '
                SELECT history_arr
                FROM '.$this->table.'
                WHERE destination_id = :destination_id AND history_arr != "null"
            ';
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':destination_id', $this->destination_id);
            $stmt->execute();
            $num =  $stmt->rowCount();

            if($num > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $data = json_decode($row['history_arr']);
                    for($i=0;count($data)>$i;$i++) {
                        $ids = $data[$i][0]->id;
                        $codes = $data[$i][0]->code;
                        $total_cost = 0;
                        $total_time = 0;
                        $start = false;
                        $ind = 0;
                        if(in_array($this->origin_id, $ids)) {
                            for($x=0;count($ids)-1>$x;$x++) {
                                if($ids[$x] == $this->origin_id) {
                                    $start = true;
                                    $ind = $x;
                                } 
                                if($start) {
                                    $init_data = $this->dig($ids[$x], $ids[$x+1]);
                                    $total_cost += $init_data[0];
                                    $total_time += $init_data[1];
                                }
                            }
                            $codes_arr = explode("-", $codes);
                            $codes_fin = array();
                            for($y=0;count($codes_arr)>$y;$y++) {
                                if($y >= $ind) {
                                    array_push($codes_fin, $codes_arr[$y]);
                                }
                            }
                            array_push($arr_av_routes, array(
                                "cost" => $total_cost,
                                "time" => $total_time,
                                "route" => implode('-', $codes_fin)
                            ));
                        }
                        
                    }
                    
                }
                $this->get_fastest_route($arr_av_routes);
            } else {
                echo "No routes available";
                
            }
        }

        public function dig($origin, $destination) {
            $query = '
                SELECT cost, time_
                FROM '.$this->table.'
                WHERE origin_id = :origin_id AND destination_id = :destination_id
            ';
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':origin_id', $origin);
            $stmt->bindParam(':destination_id', $destination);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return array($row['cost'],$row['time_']);
        }

        public function get_fastest_route($routes) {
            $fastest = 0;
            for($i=0;count($routes)>$i;$i++){
                if($i==0) {
                    $fastest = 0;
                }
                if($routes[$i]['time'] < $routes[$fastest]['time']) {
                    $fastest = $i;
                } else if($routes[$i]['time'] == $routes[$fastest]['time']) {
                    if($routes[$i]['cost'] < $routes[$fastest]['cost']) {
                        $fastest = $i;
                    }
                }
            }

            echo json_encode(array(
                'fastest_route'     =>  $routes[$fastest],
                'available_routes'  => $routes
            ));
        }

        public function create() {
            if($this->is_exists('o_d')) {
                return array(
                    'status'    =>  false,
                    'message'   =>  'Route already exists.'
                );
            } else {
                if($this->is_admin()) {
                    $this->origin_id        =   htmlspecialchars(strip_tags($this->origin_id));
                    $this->destination_id   =   htmlspecialchars(strip_tags($this->destination_id));
                    $this->cost             =   htmlspecialchars(strip_tags($this->cost));
                    $this->time_            =   htmlspecialchars(strip_tags($this->time_));
                    $this->username         =   htmlspecialchars(strip_tags($this->username));

                    $routes = $this->get_current_route($this->origin_id);
                    $num = $routes->rowCount();
                    $history_arr_init = array();
                    if($num > 0) {
                        while ($row = $routes->fetch(PDO::FETCH_ASSOC)) {
                            extract($row);
                            if(empty($row['history_arr'])) {
                                $history_item = array(
                                    'id'    =>  array(),
                                    'code'  =>  ''
                                );
                                array_push($history_item['id'], $row['origin_id'], $row['destination_id'], (int)$this->destination_id);
                                $code = $this->get_name($row['origin_id']).'-'.$this->get_name($row['destination_id']).'-'.$this->get_name($this->destination_id);
                                $history_item['code'] = $code;
                                array_push($history_arr_init, array($history_item));
                            } else {
                                $raw = json_decode($row['history_arr']);
                                $name_ = $this->get_name($this->destination_id);
                                for($i=0;count($raw)>$i;$i++) {
                                    array_push($raw[$i][0]->id, (int)$this->destination_id);
                                    $in_code = $raw[$i][0]->code.'-'.$name_;
                                    array_push($history_arr_init, [array(
                                        'id'    => $raw[$i][0]->id, 
                                        'code'  => $in_code)]
                                    );
                                }
                            }
                        }
                    }
                    $arr_history = count($history_arr_init)>0 ? json_encode($history_arr_init) : null;
                    
                    $query = '
                        INSERT INTO '.$this->table.'
                        SET 
                            name            =   :name,
                            origin_id       =   :origin_id,
                            destination_id  =   :destination_id,
                            cost            =   :cost,
                            time_           =   :time_,
                            history_arr     =   :history_arr
                    ';
                    $stmt = $this->conn->prepare($query);
                    $route_name = $this->get_name($this->origin_id).'-'.$this->get_name($this->destination_id);
                    $stmt->bindParam(':name', $route_name);
                    $stmt->bindParam(':origin_id', $this->origin_id);
                    $stmt->bindParam(':destination_id', $this->destination_id);
                    $stmt->bindParam(':cost', $this->cost);
                    $stmt->bindParam(':time_', $this->time_);
                    $stmt->bindParam(':history_arr', $arr_history);
                    if($stmt->execute()) {
                        return array(
                            'status'    =>  true,
                            'message'   =>  'Route created.'
                        );
                    } else {
                        return array(
                            'status'    =>  false,
                            'message'   =>  'Error %s \n', $stmt->error
                        );
                    }
                } else {
                    return array(
                        'status'    =>  false,
                        'message'   =>  'User is not admin.'
                    );
                }
            }
        }

        public function update() {
            if($this->is_exists('route')) {
                if($this->is_admin()) {
                    $this->origin_id        =   htmlspecialchars(strip_tags($this->origin_id));
                    $this->destination_id   =   htmlspecialchars(strip_tags($this->destination_id));
                    $this->cost             =   htmlspecialchars(strip_tags($this->cost));
                    $this->time_            =   htmlspecialchars(strip_tags($this->time_));
                    $this->username         =   htmlspecialchars(strip_tags($this->username));

                    $routes = $this->get_current_route($this->origin_id);
                    $num = $routes->rowCount();
                    $history_arr_init = array();
                    if($num > 0) {
                        while ($row = $routes->fetch(PDO::FETCH_ASSOC)) {
                            extract($row);
                            if(empty($row['history_arr'])) {
                                $history_item = array(
                                    'id'    =>  array(),
                                    'code'  =>  ''
                                );
                                array_push($history_item['id'], $row['origin_id'], $row['destination_id'], (int)$this->destination_id);
                                $code = $this->get_name($row['origin_id']).'-'.$this->get_name($this->destination_id);
                                $history_item['code'] = $code;
                                array_push($history_arr_init, array($history_item));
                            } else {
                                $raw = json_decode($row['history_arr']);
                                $name_ = $this->get_name($this->destination_id);
                                for($i=0;count($raw)>$i;$i++) {
                                    array_push($raw[$i][0]->id, (int)$this->destination_id);
                                    $in_code = $raw[$i][0]->code.'-'.$name_;
                                    array_push($history_arr_init, [array(
                                        'id'    => $raw[$i][0]->id, 
                                        'code'  => $in_code)]
                                    );
                                }
                            }
                        }
                    }
                    $arr_history = count($history_arr_init)>0 ? json_encode($history_arr_init) : null;
                    
                    $query = '
                        UPDATE '.$this->table.'
                        SET 
                            name            =   :name,
                            origin_id       =   :origin_id,
                            destination_id  =   :destination_id,
                            cost            =   :cost,
                            time_           =   :time_,
                            history_arr     =   :history_arr
                        WHERE
                            id              =   :id
                    ';
                    $stmt = $this->conn->prepare($query);
                    $route_name = $this->get_name($this->origin_id).'-'.$this->get_name($this->destination_id);
                    $stmt->bindParam(':name', $route_name);
                    $stmt->bindParam(':origin_id', $this->origin_id);
                    $stmt->bindParam(':destination_id', $this->destination_id);
                    $stmt->bindParam(':cost', $this->cost);
                    $stmt->bindParam(':time_', $this->time_);
                    $stmt->bindParam(':history_arr', $arr_history);
                    $stmt->bindParam(':id', $this->id);
                    if($stmt->execute()) {
                        return array(
                            'status'    =>  true,
                            'message'   =>  'Route updated.'
                        );
                    } else {
                        return array(
                            'status'    =>  false,
                            'message'   =>  'Error %s \n', $stmt->error
                        );
                    }
                } else {
                    return array(
                        'status'    =>  false,
                        'message'   =>  'User is not admin.'
                    );
                }
            } else {
                return array(
                    'status'    =>  false,
                    'message'   =>  'Routes does not exists.'
                );
            }
        }

        public function delete() {
            if($this->is_exists('route')) {
                if($this->is_admin()) {
                    $query = '
                        SELECT destination_id 
                        FROM '.$this->table.'
                        WHERE id = :id
                    ';
                    $stmt = $this->conn->prepare($query);
                    $stmt->bindParam(':id', $this->id);
                    $stmt->execute();
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    $dest_id = $row['destination_id'];

                    $query = '
                        DELETE FROM '.$this->table.'
                        WHERE id = :id
                    ';
                    $stmt = $this->conn->prepare($query);
                    $stmt->bindParam(':id', $this->id);
                    if($stmt->execute()) {
                        $query = '
                            SELECT id, history_arr
                            FROM '.$this->table.'
                            WHERE history_arr != "null"
                        ';
                        $stmt = $this->conn->prepare($query);
                        $stmt->execute();
                        $num = $stmt->rowCount();
                        if($num > 0) {
                            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                extract($row);
                                $this->delete_update($row, $dest_id);
                            }
                            return array(
                                'status'    =>  false,
                                'message'   =>  'Route deleted'
                            );
                        } else {
                            return array(
                                'status'    =>  false,
                                'message'   =>  'Route deleted.'
                            );
                        }
                    } else {
                        return array(
                            'status'    =>  false,
                            'message'   =>  'Error %s \n', $stmt->error
                        );
                    }
                } else {
                    return array(
                        'status'    =>  false,
                        'message'   =>  'User is not admin'
                    );
                }
            } else {
                return array(
                    'status'    =>  false,
                    'message'   =>  'Route does not exists.'
                );
            }
        }

        public function delete_update($row, $dest_id) {
            $his_arr = json_decode($row['history_arr']);
            $id_ = $row['id'];
            $up  = false;
            for($f=0;count($his_arr)>$f;$f++) {
                if(in_array($dest_id, $his_arr[$f][0]->id)) {
                    unset($his_arr[$f]);
                    $up = true;
                }
            }
            if($up) {
                $query = '
                    UPDATE '.$this->table.'
                    SET history_arr = :history_arr
                    WHERE id = :id
                ';
                $his_arr = count($his_arr)>0 ? $his_arr : null;
                $stmt = $this->conn->prepare($query);
                $stmt->bindParam(':history_arr', $his_arr);
                $stmt->bindParam(':id', $id_);
                $stmt->execute();
                return true;
            } else {
                return false;
            }
        }

        public function get_current_route($id) {
            $query = '
                SELECT history_arr, origin_id, destination_id
                FROM '.$this->table.'
                WHERE destination_id = :destination_id
            ';
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':destination_id', $id);
            $stmt->execute();
            return $stmt;
        }

        public function get_name($id) {
            $query = '
                SELECT name FROM points WHERE id = :id
            ';
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row['name'];
        }

        public function is_exists($type) {
            if($type == 'route') {
                $query = '
                    SELECT 
                        id
                    FROM
                        '.$this->table.'
                    WHERE
                        id  =   :id
                ';
                $stmt = $this->conn->prepare($query);
                $stmt->bindParam(':id', $this->id);
            } else if($type == 'o_d') {
                $query = '
                    SELECT 
                        id
                    FROM
                        '.$this->table.'
                    WHERE
                        origin_id       =   :origin_id AND
                        destination_id  =   :destination_id
                ';
                $stmt = $this->conn->prepare($query);
                $stmt->bindParam(':origin_id', $this->origin_id);
                $stmt->bindParam(':destination_id', $this->destination_id);
            }
            
            $stmt->execute();

            $num = $stmt->rowCount();
            if($num == 1) {
                return true;
            } else {
                return false;
            }
        }

 
        public function is_admin() {
            $query = '
                SELECT 
                    role
                FROM
                    users
                WHERE
                    username = :username
            ';
            $stmt = $this->conn->prepare($query);
            $this->username   =   htmlspecialchars(strip_tags($this->username));
            $stmt->bindParam(':username', $this->username);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $role   =   $row['role'];

            if($role == 1) {
                return true;
            } else {
                return false;
            }
        }
    }