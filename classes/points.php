<?php
    class Point {
        private $conn;
        private $table = 'points';

        public $id;
        public $name;
        public $username;

        public function __construct($db){
            $this->conn = $db;
        }

        public function is_exists($type) {
            if($type == 'create') {
                $query = '
                    SELECT 
                        id
                    FROM
                        '.$this->table.'
                    WHERE
                        name = :name
                ';
                $stmt = $this->conn->prepare($query);
                $this->name   =   htmlspecialchars(strip_tags($this->name));
                $stmt->bindParam(':name', $this->name);
            } else if($type == 'update' || $type == 'delete' || $type == 'read') {
                $query = '
                    SELECT 
                        id
                    FROM
                        '.$this->table.'
                    WHERE
                        id = :id
                ';
                $stmt = $this->conn->prepare($query);
                $this->id   =   htmlspecialchars(strip_tags($this->id));
                $stmt->bindParam(':id', $this->id);
            }
            $stmt->execute();

            $num = $stmt->rowCount();
            if($num == 1) {
                return true;
            } else {
                return false;
            }
        }

        public function is_admin() {
            $query = '
                SELECT 
                    role
                FROM
                    users
                WHERE
                    username = :username
            ';
            $stmt = $this->conn->prepare($query);
            $this->username   =   htmlspecialchars(strip_tags($this->username));
            $stmt->bindParam(':username', $this->username);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $role   =   $row['role'];

            if($role == 1) {
                return true;
            } else {
                return false;
            }
        }

        public function create() {
            if($this->is_exists('create')) {
                return array(
                    'status'    =>  false,
                    'message'   =>  'Point exists.'
                );
            } else {
                if($this->is_admin()) {
                    $query = '
                        INSERT INTO
                            '.$this->table.'
                        SET
                            name    =   :name
                    ';
                    $stmt = $this->conn->prepare($query);
                    $this->name    =   htmlspecialchars(strip_tags($this->name));
                    $stmt->bindParam(':name', $this->name);
                    if($stmt->execute()) {
                        return array(
                            'status'    =>  true,
                            'message'   =>  'Point created.'
                        );
                    } else {
                        return array(
                            'status'    =>  false,
                            'message'   =>  'Error %s \n', $stmt->error
                        );
                    }
                } else {
                    return array(
                        'status'    =>  false,
                        'message'   =>  'User is not admin'
                    );
                }
            }
        }

        public function update() {
            if($this->is_exists('update')) {
                if($this->is_admin()) {
                    $query = '
                        UPDATE
                            '.$this->table.'
                        SET
                            name    =   :name
                        WHERE 
                            id      =   :id
                    ';
                    $stmt = $this->conn->prepare($query);
                    $this->name    =   htmlspecialchars(strip_tags($this->name));
                    $this->id    =   htmlspecialchars(strip_tags($this->id));
                    $stmt->bindParam(':name', $this->name);
                    $stmt->bindParam(':id', $this->id);
                    if($stmt->execute()) {
                        return array(
                            'status'    =>  true,
                            'message'   =>  'Point updated.'
                        );
                    } else {
                        return array(
                            'status'    =>  false,
                            'message'   =>  'Error %s \n', $stmt->error
                        );
                    }
                } else {
                    return array(
                        'status'    =>  false,
                        'message'   =>  'User is not admin'
                    );
                }
            } else {
                return array(
                    'status'    =>  false,
                    'message'   =>  'Point does not exists.'
                );
            }
        }

        public function delete() {
            if($this->is_exists('delete')) {
                if($this->is_admin()) {
                    $query = '
                        DELETE FROM
                            '.$this->table.'
                        WHERE
                            id  =   :id
                    ';
                    
                    $stmt = $this->conn->prepare($query);
                    $this->id   =   htmlspecialchars(strip_tags($this->id));
                    $stmt->bindParam(':id', $this->id);
                    if($stmt->execute()) {
                        return array(
                            'status'    =>  true,
                            'message'   =>  'Point deleted.'
                        );
                    } else {
                        return array(
                            'status'    =>  false,
                            'message'   =>  'Error %s \n', $stmt->error
                        );
                    }
                } else {
                    return array(
                        'status'    =>  false,
                        'message'   =>  'User is not admin'
                    );
                }
            } else {
                return array(
                    'status'    =>  false,
                    'message'   =>  'Point does not exists.'
                );
            }
        }

        public function read() {
            if($this->is_exists('read')) {
                if($this->is_admin()) {
                    $query = '
                        SELECT 
                            name
                        FROM
                            '.$this->table.'
                        WHERE
                            id  =   :id
                        LIMIT 1
                    ';
                    $stmt = $this->conn->prepare($query);
                    $stmt->bindParam(':id', $this->id);
                    if($stmt->execute()) {
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        return array(
                            'status'    =>  true,
                            'point_name'    =>  $row['name']
                        );
                    } else {
                        return array(
                            'status'    =>  false,
                            'message'   =>  'Error %s \n', $stmt->error
                        );
                    }
                } else {
                    return array(
                        'status'    =>  false,
                        'message'   =>  'User is not admin.'
                    );
                }
            } else {
                return array(
                    'status'    =>  false,
                    'message'   =>  'Point does not exists.'
                );
            }
        }

        public function read_all() {
            $query = '
                SELECT *
                FROM
                    '.$this->table.'
            ';
            $stmt = $this->conn->prepare($query);
            if($stmt->execute()) {
                $num = $stmt->rowCount();
                if($num > 0) {
                    $point_arr = array(
                        'status'    => true,
                        'data'      => array()
                    );
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        extract($row);
                        $name   = $row['name'];
                        $id     = $row['id'];
                        $point_item = array(
                            'id'                =>  $id,
                            'name'              =>  $name
                        );
                        array_push($point_arr['data'], $point_item);
                    }
                    return $point_arr;
                } else {
                    return array(
                        'status'    =>  false,
                        'message'   =>  'No points found.'
                    );
                }
            } else {
                return array(
                    'status'    =>  false,
                    'message'   =>  'Error %s \n', $stmt->error
                );
            }
        }
    }