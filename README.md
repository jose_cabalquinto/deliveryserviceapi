## Current Points id
A - 1
B - 8
C - 2
D - 5
E - 4
F - 6
G - 9
H - 3
I - 7



## Create a Point
ENDPOINT	:	http://localhost/deliveryserviceapi/api/points/create.php
BODY DATA	:	name , username
			e.g {
					"name":"Z",
					"username":"admin"
				}
NOTE		:	


## View single point
ENDPOINT	:	http://localhost/deliveryserviceapi/api/points/read.php?id=9
PARAMS DATA	:	id = 9 
NOTE		:	


## View all points
ENDPOINT	:	http://localhost/deliveryserviceapi/api/points/read_all.php


## Update point
ENDPOINT	:	http://localhost/deliveryserviceapi/api/points/update.php
BODY DATA	:	name, id, username
			e.g {
					"name":"X",
					"id"	: "1",
					"username":"admin"
				}
NOTE		:	


## Delete point
ENDPOINT	:	http://localhost/deliveryserviceapi/api/points/delete.php?id=13&username=admin
PARAMS DATA	:	id, username
NOTE		:	
				
				
				
## Create Route
ENDPOINT	:	http://localhost/deliveryserviceapi/api/routes/create.php
BODY DATA	:	origin_id, destination_id, cost, time_, username
			e.g {
					"origin_id"		:	"9",
					"destination_id":	"8",
					"cost"			:	"74",
					"time_"			:	"64",
					"username"		:	"admin"
				}
NOTE		:	origin_id and destination_id is based on the ids found at points table, make sure id exists


## Read routes
ENDPOINT	:	http://localhost/deliveryserviceapi/api/routes/read.php?origin_id=1&destination_id=8
PARAMS DATA	:	origin_id, destination_id


## Update Routes
ENDPOINT	:	http://localhost/deliveryserviceapi/api/routes/update.php
BODY DATA	:	origin_id, destination_id, cost, time_, username, id
			e.g {
					"origin_id"		:	"9",
					"destination_id":	"8",
					"cost"			:	"73",
					"time_"			:	"164",
					"username"		:	"admin",
					"id"			:	"43"
				}
				
				
## Delete Routes
END POINT	:	http://localhost/deliveryserviceapi/api/routes/delete.php?id=1&username=admin
PARAMS DATA	:	id, username